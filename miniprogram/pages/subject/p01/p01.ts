// pages/subject/p01/p01.ts
import sub from "../../../api/subject/subject"

Page({

  /**
   * 页面的初始数据
   */
  data: {
    // sub: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if ((Object.keys(options).length) !== 0) {
      if (options.id !== undefined) {
        let key = 'id' + parseInt(options.id)
        let title: string|undefined|null = ''
        switch (key) {
          case 'id1':
            this.setData({
              sub: sub.id1
            })
            title = sub.id1.title
            break;
          case 'id4':
            this.setData({
              sub: sub.id4
            })
            title = sub.id4.title
            break;
          case 'id5':
            this.setData({
              sub: sub.id5
            })
            title = sub.id5.title
            break;
          case 'id6':
            this.setData({
              sub: sub.id6
            })
            title = ''
            break;
          case 'id7':
            this.setData({
              sub: sub.id7
            })
            title = sub.id7.title
            break;
          case 'id8':
            this.setData({
              sub: sub.id8
            })
            title = sub.id8.title
            break;
          case 'id9':
            this.setData({
              sub: sub.id9
            })
            title = sub.id9.title
            break;
          case 'id10':
            this.setData({
              sub: sub.id10
            })
            title = sub.id10.title
            break;
          default:
            wx.navigateBack();
        }
        wx.setNavigationBarTitle({
          title: title ? title : '刷题小抄'
        });
      }
    } else {
      wx.navigateBack();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})