export default {
  'code': 0,
  'msg': 'ok',
  'data': [
    {
      'id': 1,
      'title': '罚款知识点（单值）',
      'type': 1
    },
    {
      'id': 2,
      'title': '罚款知识点（区间）',
      'type': 2
    },
    {
      'id': 3,
      'title': '罚款知识点（倍数+上限）',
      'type': 2
    },
    {
      'id': 4,
      'title': '驾照暂扣、收回、吊销知识点',
      'type': 1
    },
    {
      'id': 5,
      'title': '封考年限知识点',
      'type': 1
    },
    {
      'id': 7,
      'title': '扣分知识点',
      'type': 1
    },
    {
      'id': 8,
      'title': '扣分知识点（载人超载）',
      'type': 1
    },
    {
      'id': 9,
      'title': '扣分知识点（超速）',
      'type': 1
    },
    {
      'id': 10,
      'title': '扣分知识点（载货超重）',
      'type': 1
    },
  ]
}