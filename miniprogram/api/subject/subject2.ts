export default {
  'id2': {
    'title': '罚款知识点（区间）',
    'list': [
      {
        'start': '20',
        'end': '200',
        'item': [
          '大型中型客车、重型挂车、公交车、大型货车，信息变更超30天未备案：罚款20~200。',
          '补证还用旧证：没收旧证，罚20~200。',
          '实习期牵引挂车：罚20~200。',
          '道路上学车，未按照路线、时间、专用标识的规定进行：罚教员20~200。',
          '实习期开公共汽车、营运客车或者执行任务的警车、消防车、救护车、工程救险车以及载有爆炸物品、易燃易爆化学物品、剧毒或者放射性等危险物品的机动车：罚20~200。',
        ]
      },
      {
        'start': '200',
        'end': '500',
        'item': [
          '身体条件变化，仍开车：收回驾照，罚200~500。',
        ]
      },
      {
        'start': '200',
        'end': '2000',
        'item': [
          '学车没教练：罚200~2000。',
          '违反交通管制，强行通行，不听劝阻：罚200~2000。',
          '故意损毁、移动、涂改交通设施，造成事故，尚不构成犯罪：罚200~2000。',
          '驾驶拼装车、报废车：收缴，强制报废，罚款200~2000，吊销驾照。',
          '非法装警报器、标志灯：强拆收缴，罚200~2000。',
          '无驾照、被吊销驾照、被暂扣驾照的开车：罚200~2000，可以并处拘留15日以下。',
          '交通肇事逃逸，尚不构成犯罪：罚200~2000，可以并处拘留15日以下。',
          '强迫他人违反交规，造成事故，尚不构成犯罪：罚200~2000，可以并处拘留15日以下。',
          '非法拦截、扣留机动车辆，不听劝阻，造成交通严重阻塞或者较大财产损失：罚200~2000，可以并处拘留15日以下。',
          '车交给无驾照、被吊销驾照、被暂扣驾照的人开：罚200~2000，可以并处吊销驾照。',
          '超过限速50%：罚200~2000，可以并处吊销驾照。',
        ]
      },
      {
        'start': '1000',
        'end': '2000',
        'item': [
          '初犯饮酒开车：暂扣驾照6个月，罚1000~2000。',
          '饮酒开车后，再犯：拘留10日以下，罚1000~2000，吊销驾照。',
        ]
      },
      {
        'start': '2000',
        'end': '5000',
        'item': [
          '使用或伪造、变造，登记证书、号牌、行驶证、驾驶证：收缴，扣车，拘留15日以下，罚2000~5000，构成犯罪的，依法追究刑事责任。',
        ]
      },
    ]
  },
  'id3': {
    'title': '罚款知识点（倍数+上限）',
    'list': [
      {
        'start': 'x3-',
        'middle': '但',
        'end': '2w-',
        'item': [
          '组织作假和代替审验教育学习，有违法所得：罚所得3倍以下，但最高不超过2w。'
        ]
      },
      {
        'start': 'x3-',
        'middle': '但',
        'end': '5w-',
        'item': [
          '代人交通违法背锅（处罚和记分），并收钱：罚所得3倍以下，但最高不超过5w。'
        ]
      },
      {
        'start': 'x5-',
        'middle': '但',
        'end': '10w-',
        'item': [
          '组织违法背锅牟利：罚所得5倍以下，但最高不超过10w。'
        ]
      },
    ]
  }
}