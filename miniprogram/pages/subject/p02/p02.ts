// pages/subject/p02/p02.ts
import sub from '../../../api/subject/subject2'
Page({

  /**
   * 页面的初始数据
   */
  data: {
    sub: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    if ((Object.keys(options).length) !== 0) {
      if (options.id !== undefined) {
        let key = 'id' + parseInt(options.id)
        let title: string|undefined|null = ''
        switch(key) {
          case 'id2':
            this.setData({
              sub: sub.id2
            })
            title = sub.id2.title
            break;
          case 'id3':
            this.setData({
              sub: sub.id3
            })
            title = sub.id3.title
            break;
          default:
            wx.navigateBack();
        }
        wx.setNavigationBarTitle({
          title: title ? title : '刷题小抄'
        });
      }
    } else {
      wx.navigateBack();
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})