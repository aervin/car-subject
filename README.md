刷题小抄
===

微信原生小程序 + TypeScript + Sass + JSON

> 这是一个刷题笔记辅助工具，让刷题直观，好记忆，是微信小程序，随时随地方便快速使用。
> 
> 项目解决了驾考科一刷题难点知识可以直观舒适阅读的问题。

## 项目介绍

刷题小抄，一个微信原生小程序，可以纯前端发布。

项目已开源，只可用在学习，不可商用。

主要功能：

- **驾考科一** - 难点知识
- **处罚题** - 直观展示阅读
- **扣分题** - 直观展示阅读

## 开发环境

微信原生小程序 + TypeScript + Sass + JSON 。页面逻辑采用UI交互和数据分离模式，确保了项目的后续开发扩展。

数据分离为本地JSON文件结构，可纯前端方式上线发布，也为转服务器接口数据方式发布提供后续扩展。

## 快速使用

线上版本，可在微信搜索“刷题小抄”小程序。

本地运行，通过文件 `.example.project.config.json`、`.example.project.private.config.json`，修改还原文件 `project.config.json`、`project.private.config.json`，再使用微信开发者工具导入项目运行。

## 开源许可证

[![License](https://img.shields.io/badge/license-GPL-blue.svg)](LICENSE)
