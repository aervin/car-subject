// 准备
import subject_list from './subject/list'
// 接口请求
let subDomain = ''  // 子域，没有为空
const API_BASE_URL = 'http:127.0.0.1' // 主域名，默认http:127.0.0.1

// 真实的接口数据
// const request = (url: string, method: string, data: object) => {
//   let _url = API_BASE_URL + subDomain + url
//   return _url
// }

// 模拟接口数据，上线时，改为走真实接口
const api = (url: string, method: string, data: object) => {
  let _url = API_BASE_URL + subDomain + url
  if (method == 'post') {
    return {
      'code': 0,
      'msg': '操作成功',
      'data': [
        {'url': _url, 'data': data}
      ]
    }
  }
  return subject_list
}

module.exports = {
  api,
  // 题目列表接口数据
  getList: (data: any) => api('subject_list', 'get', data),
  // 题目详情接口数据
  getDetail: (data: object) => api('subject_detail', 'get', data),
}